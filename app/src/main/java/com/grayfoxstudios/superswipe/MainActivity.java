package com.grayfoxstudios.superswipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Random;

import android.media.SoundPool;
import android.media.MediaPlayer;

public class MainActivity extends AppCompatActivity {

    private ImageView arrowImage;
    private RelativeLayout parentLayout, gameMenu;
    private TextView highScoreTextView, scoreTextView, countDownTextView, menuTitle;
    private Button startButton, restartButton;
    private Resources resources;
    private GestureDetector gestureDetector;
    private Random g;
    private String direction; //"up", "left", "down", or "right"
    private final int STARTING_COUNTDOWN = 30;
    private boolean running = false;
    private int highScore, score = 0;
    private SharedPreferences preferences;
    private String highScoreText, scoreText;
    private SSCountDownTimer countDownTimer;
    private Context context;
    private Animation up, left, down, right;
    private long millisecondsLeft = 30000;
    private static final String PREFS_NAME = "SuperSwipePrefs";
    private HashMap<String, Bitmap> arrows;
    //Sound Manager
    private SoundPool sp = new SoundPool(1, AudioManager.STREAM_MUSIC,0);
    private static int soundsIds[] = new int[3];
    private static MediaPlayer mp;
    private static Switch musicOnOff;
    private static boolean musicRunning = true;
    private int onOffCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }

        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        gameMenu = (RelativeLayout) findViewById(R.id.gameMenu);
        arrowImage = (ImageView) findViewById(R.id.arrowImage);
        highScoreTextView = (TextView) findViewById(R.id.highScoreTextView);
        scoreTextView = (TextView) findViewById(R.id.scoreTextView);
        countDownTextView = (TextView) findViewById(R.id.countDownTextView);
        menuTitle = (TextView) findViewById(R.id.menuTitle);
        startButton = (Button) findViewById(R.id.startButton);
        restartButton = (Button) findViewById(R.id.restartButton);
        restartButton.setVisibility(View.GONE);

        context = getApplicationContext();
        resources = getResources();
        highScoreText = getString(R.string.high_score_plus);
        scoreText = getString(R.string.score_plus);

        preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        highScore = preferences.getInt("highScore", 0);
        String string = highScoreText + " " + highScore;
        highScoreTextView.setText(string);

        arrows = new HashMap<>();
        arrows.put("up", BitmapFactory.decodeResource(resources, R.drawable.art_arrow_up));
        arrows.put("left", BitmapFactory.decodeResource(resources, R.drawable.art_arrow_left));
        arrows.put("down", BitmapFactory.decodeResource(resources, R.drawable.art_arrow_down));
        arrows.put("right", BitmapFactory.decodeResource(resources, R.drawable.art_arrow_right));

        up = AnimationUtils.loadAnimation(context, R.anim.up);
        left = AnimationUtils.loadAnimation(context, R.anim.left);
        down = AnimationUtils.loadAnimation(context, R.anim.down);
        right = AnimationUtils.loadAnimation(context, R.anim.right);
        up.setAnimationListener(new SwipeAnimationListener());
        left.setAnimationListener(new SwipeAnimationListener());
        down.setAnimationListener(new SwipeAnimationListener());
        right.setAnimationListener(new SwipeAnimationListener());

        gestureDetector = new GestureDetector(new SSGestureDetector());
        g = new Random();
        arrowImage.setVisibility(View.GONE);

        parentLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        //adding the sound
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundsIds[0] = sp.load(context, R.raw.swish, 1);
        soundsIds[1] = sp.load(context, R.raw.wrongswipe, 1);
        soundsIds[2] = sp.load(context, R.raw.swish2,1 );
        musicOnOff = (Switch) findViewById(R.id.musicControl);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                running = true;
                showMenu(false);
                if (startButton.getText().toString().equals(getString(R.string.start))) {
                    setDirection();
                    arrowImage.setVisibility(View.VISIBLE);
                    millisecondsLeft = 30000;
                    countDownTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                    String string = scoreText +  " " + score;
                    scoreTextView.setText(string);
                }
                countDownTimer = new SSCountDownTimer(millisecondsLeft, 10);
                countDownTimer.start();
                //This looks a little weird, but is necessary to keep from throwing null pointers
                //and also to only play music if music is turned on.
                if(mp == null && musicRunning && onOffCount == 10){
                    mp = MediaPlayer.create(context, R.raw.secretsong);
                    mp.setVolume(.75f, .75f);
                }

                else if(mp == null && musicRunning){
                    mp = MediaPlayer.create(context, R.raw.modernhousebeatrevised);
                    mp.setVolume(.75f, .75f);
                }
                if(musicRunning)
                    mp.start();
            }
        });

        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                running = true;
                score = 0;
                startButton.setText(getString(R.string.start));
                startButton.callOnClick();
                if(musicRunning)
                    mp.seekTo(0);
            }
        });
        //Listener to turn music on or off
        musicOnOff.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!musicOnOff.isChecked()){//turn music off
                    if(musicRunning && mp != null) {
                        mp.stop();
                        mp = null;
                    }
                    onOffCount++;
                    musicRunning = false;
                }
                else{
                    musicRunning = true;    //turn music on
                }
            }
        });



    }

    @Override
    public void onBackPressed() {
        if (running) {
            running = false;
            menuTitle.setText(getString(R.string.paused));
            restartButton.setVisibility(View.VISIBLE);
            startButton.setText(getString(R.string.continue_text));
            countDownTimer.cancel();
            if(musicRunning)
                mp.pause();
            showMenu(true);
        }
        else {
            super.onBackPressed();
        }
    }

    private void showMenu(boolean show) {
        if (show) {
            gameMenu.setVisibility(View.VISIBLE);
        }
        else {
            gameMenu.setVisibility(View.GONE);
        }
    }

    private void endGame() {
        running = false;
        score = 0;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("highScore", highScore);
        editor.apply();
        menuTitle.setText(getString(R.string.menu));
        restartButton.setVisibility(View.GONE);
        startButton.setText(getString(R.string.start));
        //have to check that music is running before attempting to restart it.
        //Looks kind of weird setting to null, but was the only way I could get it to
        //restart with out throwing exceptions.
        if(musicRunning) {
            mp.reset();
            mp = null;
        }
        showMenu(true);
    }

    private void setDirection() {
        switch (g.nextInt(4) + 1) {
            case 1:
                direction = "up";
                arrowImage.setImageBitmap(arrows.get("up"));
                break;
            case 2:
                direction = "left";
                arrowImage.setImageBitmap(arrows.get("left"));
                break;
            case 3:
                direction = "down";
                arrowImage.setImageBitmap(arrows.get("down"));
                break;
            case 4:
                direction = "right";
                arrowImage.setImageBitmap(arrows.get("right"));
                break;
        }
    }

    private void incrementScore() {
        score++;
        String string;
        if (score >= highScore) {
            highScore = score;
            string = highScoreText + " " + highScore;
            highScoreTextView.setText(string);
        }
        string = scoreText +  " " + score;
        scoreTextView.setText(string);
    }

    class SSGestureDetector extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (running) {
                switch (getSlope(e1.getX(), e1.getY(), e2.getX(), e2.getY())) {
                    case 1:
                        //up
                        if (direction.equals("up")) {
                            incrementScore();
                            arrowImage.startAnimation(up);
                            sp.play(soundsIds[2], 3, 3, 1, 0, (float)1.0);
                            return true;
                        }
                        break;
                    case 2:
                        //left
                        if (direction.equals("left")) {
                            incrementScore();
                            arrowImage.startAnimation(left);
                            sp.play(soundsIds[0], 3, 2, 1, 0, (float)1.0);
                            return true;
                        }
                        break;
                    case 3:
                        //down
                        if (direction.equals("down")) {
                            incrementScore();
                            arrowImage.startAnimation(down);
                            sp.play(soundsIds[2], 3, 3, 1, 0, (float)1.0);
                            return true;
                        }
                        break;
                    case 4:
                        //right
                        if (direction.equals("right")) {
                            incrementScore();
                            arrowImage.startAnimation(right);
                            sp.play(soundsIds[0], 2, 3, 1, 0, (float)1.0);
                            return true;
                        }
                        break;
                }
                sp.play(soundsIds[1], 1, 1, 1, 0, (float)1.0);
                countDownTimer.cancel();
                int wrongBitmap = getCorrectDirection();
                arrowImage.setImageBitmap(BitmapFactory.decodeResource(resources, wrongBitmap));
                endGame();
            }
            return false;
        }

        private int getCorrectDirection() {
            switch (direction) {
                case "up":
                    return R.drawable.art_arrow_up_wrong;
                case "left":
                    return R.drawable.art_arrow_left_wrong;
                case "down":
                    return R.drawable.art_arrow_down_wrong;
                default:
                    return R.drawable.art_arrow_right_wrong;
            }
        }

        private int getSlope(float x1, float y1, float x2, float y2) {
            Double angle = Math.toDegrees(Math.atan2(y1 - y2, x2 - x1));
            if (angle > 45 && angle <= 135)
                // up
                return 1;
            if (angle >= 135 && angle < 180 || angle < -135 && angle > -180)
                // left
                return 2;
            if (angle < -45 && angle>= -135)
                // down
                return 3;
            if (angle > -45 && angle <= 45)
                // right
                return 4;
            return 0;
        }
    }

    class SSCountDownTimer extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public SSCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            millisecondsLeft = millisUntilFinished;
            countDownTextView.setText(Math.round(millisUntilFinished / 10.0) / 100.0 + "");
            if (millisUntilFinished <= 10000) {
                countDownTextView.setTextColor(ContextCompat.getColor(context, R.color.wrong));
            }
        }

        @Override
        public void onFinish() {
            countDownTextView.setText("00.00");
            endGame();
        }

    }

    class SwipeAnimationListener implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            setDirection();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}
