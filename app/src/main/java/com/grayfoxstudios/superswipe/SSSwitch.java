package com.grayfoxstudios.superswipe;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Switch;

/**
 * Created by hillj17 on 4/22/16.
 */
public class SSSwitch extends Switch {

    public SSSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/KillerBoots.ttf"));
    }
}
