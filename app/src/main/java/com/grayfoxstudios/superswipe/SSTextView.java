package com.grayfoxstudios.superswipe;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by hillj17 on 1/19/16.
 */
public class SSTextView extends TextView {

    public SSTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/KillerBoots.ttf"));
    }
}
